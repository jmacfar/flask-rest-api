from flask_restful import Resource, reqparse
from models.user import UserModel

class UserRegister(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument("username",
                        required=True,
                        type=str,
                        help="Username to register")
    parser.add_argument("password",
                        required=True,
                        type=str,
                        help="Password to assign to newly registered user")

    def post(self):
        data = UserRegister.parser.parse_args()

        if UserModel.find_by_username(data['username']):
            return dict(message="user already exists"), 409

        UserModel.save_to_db(UserModel(**data))

        return dict(message="user created successfully"), 201