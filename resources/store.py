from flask_restful import Resource
from models.store import StoreModel

class Store(Resource):
    def get(self, name):
        store = StoreModel.find_store_by_name(name)
        if store:
            return store.json()
        else:
            return dict(message='store does not exist'), 404

    def post(self, name):
        if StoreModel.find_store_by_name(name):
            return dict(message='store already exists'), 400

        store = StoreModel(name)

        try:
            store.save_to_db()
        except:
            return dict(message='An error occured saving store to db'), 500

        return store.json(), 201


    def delete(self, name):
        store = StoreModel.find_store_by_name(name)

        if store:
            store.delete_from_db()

        return dict(message='store deleted')


class StoreList(Resource):
    def get(self):
        return dict(stores=[store.json() for store in StoreModel.query.all()])