from flask import request
from flask_restful import Resource, reqparse
from flask_jwt import jwt_required
import sqlite3

from models.item import ItemModel

class Item(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('price',
                        type=float,
                        required=True,
                        help="This is the price for the item. Required."
                        )
    parser.add_argument('store_id',
                        type=int,
                        required=True,
                        help="This is the store_id for the item. Required."
                        )
    @jwt_required()
    def get(self, name):
        item = ItemModel.find_item_by_name(name)
        if item:
            return item.json()

        return dict(message="item does not exist"), 404

    def post(self, name):
        if ItemModel.find_item_by_name(name):
            return dict(message="item already exists"), 400

        data = self.parser.parse_args()
        item = ItemModel(name, **data)
        item.save_to_db()

        return item.json(), 201

    def put(self, name):
        data = self.parser.parse_args()
        item = ItemModel.find_item_by_name(name)

        if item:
            item.price = data['price']
            item.store_id = data['store_id']
        else:
            item = ItemModel(name, **data)

        item.save_to_db()

        return item.json()

    def delete(self, name):
        item = ItemModel.find_item_by_name(name)
        if item:
            item.delete_from_db()

        return dict(message="item deleted")


class ItemList(Resource):
    def get(self):
        return {'items': list(map(lambda x: x.json(), ItemModel.query.all()))}
